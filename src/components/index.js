// 这里是组件库作者进行封装的地方
import PageTools from '@/components/PageTools'
import UploadExcel from '@/components/UploadExcel'
import ImageUpload from '@/components/ImageUpload'
import ScreenFull from '@/components/ScreenFull'
import ThemePicker from '@/components/ThemePicker'
import Lang from '@/components/Lang'
import TagsView from '@/components/TagsView'

export default {
  install(Vue) {
    // 这个函数会在调用 Vue.use 的时候自动执行
    // 而且有一个形参, Vue 构造函数本身
    // 想要注册什么组件, 做什么准备
    // 在这里写即可
    Vue.component('PageTools', PageTools)
    Vue.component('UploadExcel', UploadExcel)
    Vue.component('ImageUpload', ImageUpload)
    Vue.component('ScreenFull', ScreenFull)
    Vue.component('ThemePicker', ThemePicker)
    Vue.component('Lang', Lang)
    Vue.component('TagsView', TagsView)
  }
}

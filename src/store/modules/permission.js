import { constantRoutes, asyncRoutes } from '@/router'

const state = {
  routes: []
}

const mutations = {
  setRoutes(state, data) {
    state.routes = [...constantRoutes, ...data]
  }
}

const actions = {
  filterRoutes(store, menus) {
    // 1. 全部路由(可以引入)
    // 2. 当前员工的所有数据(在别的模块,可以考虑, 调用时传入)
    // 最后过滤出有权限的路由
    const routes = asyncRoutes.filter(route => menus.includes(route.name))
    // 以上是经过筛选的动态路由, 只剩下有权限访问的对象
    // 将这些对象跟静态路由合并, 就是当前这个人可以访问的所有菜单
    // 设置一个 mutations 放入 state 当中方便渲染
    store.commit('setRoutes', routes)

    return routes
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

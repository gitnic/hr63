import { login, getUserInfo, getUserDetailById } from '@/api/user'
import { setToken, getToken, removeToken, setTimeStamp } from '@/utils/auth'
import { resetRouter } from '@/router'
const state = {
  token: getToken(),
  userInfo: {}
}
const mutations = {
  setToken(state, token) {
    state.token = token
    // 除了存入 state 以外, 还需要持久化放到浏览器中
    setToken(token)
  },
  removeToken(state) {
    state.token = null
    removeToken()
  },
  setUserInfo(state, data) {
    state.userInfo = { ...data }
  },
  removeUserInfo(state) {
    state.userInfo = {}
  }
}
const actions = {
  async login(store, data) {
    // login(data).then(res => {
    //   console.log(res)
    //   store.commit('setToken', res.data.data)
    // })
    const res = await login(data)
    console.log(res)
    // 由于在拦截器中解构了 res.data.data
    // 所以后面我们使用的所有res其实就是真正的数据
    // 不用再 res.data.data
    store.commit('setToken', res)
    setTimeStamp()
  },
  async getUserInfo(store) {
    const res = await getUserInfo()
    // 这里虽然可以获取登录账号的基本信息
    // 但是缺了头像, 拿着这个基本信息中心的员工 id
    // 获取一下这个员工的其他基本数据, 两项合并, 再存入 state 当中
    const resDetail = await getUserDetailById(res.userId)

    // 将两个接口的数据聚集在一起, 再放入 state
    const data = {
      ...res,
      ...resDetail
      // staffPhoto: res.resDetail.staffPhoto
    }
    store.commit('setUserInfo', data)
  },
  logout(store) {
    resetRouter()
    store.commit('permission/setRoutes', [], { root: true })
    store.commit('removeToken')
    store.commit('removeUserInfo')
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

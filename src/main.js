import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// import locale from 'element-ui/lib/locale/lang/en' // lang i18n

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'

import i18n from '@/lang'

import '@/icons' // icon
import '@/permission' // permission control

// set ElementUI lang to EN
// Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
Vue.use(ElementUI, {
  i18n: (key, value) => i18n.t(key, value)
})

Vue.config.productionTip = false

// 演示自定义指令的用法
import { imgerr } from '@/directives'
Vue.directive('imgerr', imgerr)

// 组件库使用者进行统一注册的地方
import MyComponents from '@/components'
Vue.use(MyComponents)
// 其实这个 use 是调用了传入对象中的 insttall 方法
// 将 Vue 构造函数传入进去
// Vue.use = (obj) => {
//   obj.install(Vue)
// }

// // 演示全局过滤器的声明方法
// import { formatDate } from '@/filters'
// Vue.filter('formatDate', formatDate)
// // 上面是引入单个函数进行过滤器注册,
// // 但是如果函数太多想要一次性完成怎么办?
// // 1. 引入所有函数
import * as filters from '@/filters'
// // 2. 遍历并且进行注册
// // 遍历的是这个对象中的 key, 每个 key 拿出对应的函数
// // 注册同名锅炉器
// console.log(filters)
// // console.log(filters.formatDate)
for (const key in filters) {
  Vue.filter(key, filters[key])
  // const filterName = key
  // const filterFunction = filters[key]
  // Vue.filter(filterName, filterFunction)
}

// 演示全局的混入
import { checkPermission } from '@/mixin'
Vue.mixin(checkPermission)

new Vue({
  el: '#app',
  router,
  store,
  i18n,
  render: h => h(App)
})

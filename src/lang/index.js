// 演示国际化的引入
import Vue from 'vue'
import VueI18n from 'vue-i18n'
import Cookie from 'js-cookie'

Vue.use(VueI18n)

// 引入饿了么语言包字典
import ElementZH from 'element-ui/lib/locale/lang/zh-CN'
import ElementEN from 'element-ui/lib/locale/lang/en'

// 引入自定义语言包
import CustomZH from './zh'
import CustomEN from './en'

export default new VueI18n({
  locale: Cookie.get('language') || 'zh',
  messages: {
    zh: {
      greeting: '你好',
      confirmDelEmployee: '是否确认删除当前员工?',
      tableTitle: {
        username: '姓名',
        avatar: '头像'
      },
      ...ElementZH,
      ...CustomZH
    },
    en: {
      greeting: 'Hello',
      tableTitle: {
        username: 'Name',
        avatar: 'Avatar'
      },
      confirmDelEmployee: 'Are you sure?',
      ...ElementEN,
      ...CustomEN
    }
  }
})

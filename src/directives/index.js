export const imgerr = {
  inserted(dom, options) {
    console.log('触发插入钩子')
    // 检查是否为空, 如果是, 直接将备用地址放上去
    dom.src = dom.src || options.value
    // console.log('监听到这个dom执行到被插入到父节点的步骤')
    // 这里是 img 标签插入页面时触发的函数
    // 如果需要处理错误图片地址, 就应该监听他的错误事件
    // 将 src 替换成默认图片
    // 这里可以接受两个参数
    // 1. dom 本身, 这里是 img
    // 2. 是当前指令的配置, 其中如果有传参, 在 value 值当中
    // console.log(dom)
    // console.log(options.value)
    dom.addEventListener('error', () => {
      dom.src = options.value
    })
  },
  componentUpdated(dom, options) {
    console.log('页面更新触发')
    dom.src = dom.src || options.value
  }
}

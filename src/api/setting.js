import request from '@/utils/request'

// 获取公司信息
export function getCompanyInfo(id) {
  return request({
    url: `/company/${id}`
  })
}

// 获取角色列表
export function getRoleList(params) {
  return request({
    url: '/sys/role',
    // 路径上的查询参数用 params 进行传递即可
    params
  })
}

// 删除角色
export function delRole(id) {
  return request({
    url: `/sys/role/${id}`,
    method: 'delete'
  })
}

// 根据 id 获取角色详情
export function getRoleDetail(id) {
  return request({
    url: `/sys/role/${id}`
  })
}

// 更新角色
export function editRole(data) {
  return request({
    method: 'put',
    url: `/sys/role/${data.id}`,
    data
  })
}

// 新增角色
export function addRole(data) {
  return request({
    url: '/sys/role',
    method: 'post',
    data
  })
}

// 分配权限
export function assignPerm(data) {
  return request({
    method: 'put',
    url: '/sys/role/assignPrem',
    data
  })
}

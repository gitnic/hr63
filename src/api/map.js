import request from '@/utils/amapRequest'

const key = '820fda2a885602fa27b6cab629768a88'

export function searchPlace(params) {
  params.key = key
  return request({
    url: '/place/text',
    params
  })
}

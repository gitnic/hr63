import axios from 'axios'

import store from '@/store'

import router from '@/router'

import { Message } from 'element-ui'

import { getTimeStamp } from './auth'

const checkTimeout = () => {
  // 1. 当前时间
  const now = Date.now()
  // 2. 之前记录的时间
  const loginTime = getTimeStamp()
  // 3. 定义一个超时时间
  const timeout = 1000 * 60 * 60 * 2
  // 4. 返回判断
  return now - loginTime > timeout
}

// 创建 axios 实例
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000 // request timeout
})

// 请求拦截器 每次请求都会触发, 可以拦截到当前请求的配置
service.interceptors.request.use(config => {
  // 处理token问题
  // 1. 如果有 token 就放入请求头
  // 2. token 格式, 最前方有个 'Bearer ' 字符串, 注意最后的空格
  if (store.getters.token) {
    // 发现有 token 不能直接加, 校验有没有超时, 如果有超时应该直接登出
    if (checkTimeout()) {
      // 退出
      console.log('超时')
      // 1. 调用 vuex 清理数据
      store.dispatch('user/logout')
      // 2. 调用 router 跳转页面
      router.push('/login')
      // 3. 停止掉当前的请求
      return Promise.reject(new Error('token 已失效'))
    }
    config.headers.Authorization = `Bearer ${store.getters.token}`
  }

  // 拦截必须放行
  return config
})

// 响应拦截器
// 响应有三种情况
service.interceptors.response.use(
  res => {
    // err1. 请求成功
    console.log('这是网络层面的成功')
    console.log(res)
    const { success, message, data } = res.data
    if (success) {
      //   1.1 数据成功 -> 返回数据
      console.log('数据也成功了')
      // return res
      // 这里不直接返回 res
      // 而是从 res.data 中 解构出 data (res.data.data)
      return data
    } else {
      //   1.2 数据失败 -> 提示错误+停止请求
      console.log('但是数据失败了')
      // 弹出错误
      Message.error(message)
      // 拒绝掉当前的请求
      return Promise.reject(new Error(message))
    }
  },
  err => {
    // 专门被动处理后端 token 失效
    // 这里面需要处理的错误, 不止 token 报错一种
    // 其他比如断网, 服务器崩溃等等也是这里处理的
    // 这里要写得更加详细, 防止 err 里面没有 response 的情况
    if (err.response && err.response.data && err.response.data.code === 10002) {
      store.dispatch('user/logout')
      router.push('/login')
    }
    // 2. 请求失败 -> 提示错误+停止请求
    console.log('这是网络层面失败')
    console.dir(err)
    // 弹出弹窗告诉用户
    Message.error(err.message)
    // 接着需要放行(报错)
    // return 123 这种普通数据的响应会返回到 .then
    // 这里应该用 Promise.reject 直接停止当前的请求过程
    return Promise.reject(new Error(err.message))
  }
)

export default service

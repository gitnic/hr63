import Cookies from 'js-cookie'

// 储存语法
// Cookies.set(key, value)
// 读取语法
// Cookies.get(key)
// 删除语法
// Cookies.remove(key)

const TokenKey = 'hrsaas-ihrm-token'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

const timeKey = 'hrsaas-timestamp-key'

export function setTimeStamp() {
  const timestamp = Date.now()
  Cookies.set(timeKey, timestamp)
}

export function getTimeStamp() {
  return Cookies.get(timeKey)
}

import router from '@/router'
import store from '@/store'
router.beforeEach(async(to, from, next) => {
  const token = store.getters.token
  const url = to.path
  const whiteList = ['/login', '/404']

  // 1. 已登录+去登录页=跳转首页
  if (token && url === '/login') {
    next('/')
  }

  // 2. 已登录+去其他页=放行
  if (token && url !== '/login') {
    // 导航守卫一共四种情况, 这里有 token 又不是去登录页
    // 就是最普遍的普通后台页面状况
    // 就应该在这获取用户资料, 获取完以后再进入页面
    // 另外只需要第一次刷新时获取, 如果数据已经存在
    // 跳转页面时, 即使触发到这个逻辑也不应该再次获取了
    // if (Object.keys(store.state.user.userInfo).length === 0) {
    if (!store.getters.userId) {
      await store.dispatch('user/getUserInfo')
      // 这里面保证已经拿到了用户数据
      // 然后又没有进入页面, 应该在这进行路由权限筛选
      const res = await store.dispatch(
        'permission/filterRoutes',
        store.state.user.userInfo.roles.menus
      )
      console.log(res)

      // 这里筛选过后, 希望可以拿到有权限的动态路由
      // 添加到配置中
      // 只要接收到路由配置, 利用路由组件自己的 addRoutes 方法即可动态添加
      router.addRoutes([
        ...res,
        { path: '*', redirect: '/404', hidden: true }
      ])

      next(to.path)
    } else {
      next()
    }
  }

  // 3. 未登录+在白名单=放行
  // if (!token && whiteList.indexOf(url) > -1) {
  if (!token && whiteList.includes(to.path)) {
    next()
  }

  // 4. 未登录+不在白名单=去登录页
  if (!token && !whiteList.includes(to.path)) {
    next('/login')
  }
})

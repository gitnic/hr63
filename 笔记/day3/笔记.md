# 登录页布局

- 标题
- 背景图
- 输入框文字和背景
- 错误提醒颜色
- 按钮的样式
- 登录的默认账号提醒文字

![image-20210627090126462](笔记.assets/image-20210627090126462.png)

# 表单校验

## 自定义校验函数

- 规则定义

  ![image-20210627094823144](笔记.assets/image-20210627094823144.png)

- 函数编写

  ![image-20210627094904381](笔记.assets/image-20210627094904381.png)

## 登录页的实际应用

- username 字段换成 mobile
- placeholder 换成 中文
- 重写校验逻辑

# 跨域

## 跨域情况

![image-20210627104137772](笔记.assets/image-20210627104137772.png)

## 解决逻辑

![image-20210627105050241](笔记.assets/image-20210627105050241.png)

## 实际修改

![image-20210627110156548](笔记.assets/image-20210627110156548.png)

![image-20210627110221369](笔记.assets/image-20210627110221369.png)

## 登录测试

![image-20210627111129487](笔记.assets/image-20210627111129487.png)

![image-20210627111140169](笔记.assets/image-20210627111140169.png)

# Vuex 管理登录逻辑

![image-20210627120451030](笔记.assets/image-20210627120451030.png)

![image-20210627120507197](笔记.assets/image-20210627120507197.png)

# async / await 演示和说明

![image-20210627121517290](笔记.assets/image-20210627121517290.png)

# Vuex token 数据持久化

这个项目中, 用到的是 Cookies 的工具

![image-20210627145308753](笔记.assets/image-20210627145308753.png)

![image-20210627145756624](笔记.assets/image-20210627145756624.png)

**getters 优化 token 的获取**

![image-20210627150303390](笔记.assets/image-20210627150303390.png)

**dispatch 调用 action 其实也是 promise, async/await 的调用方法, 使用 try/catch 捕获错误**

![image-20210627152405874](笔记.assets/image-20210627152405874.png)

# 响应拦截统一处理错误

## 三种可能性

![image-20210627161121208](笔记.assets/image-20210627161121208.png)

## 网络成功 数据成功

![image-20210627164858178](笔记.assets/image-20210627164858178.png)

## 网络成功 数据失败

![image-20210627164911907](笔记.assets/image-20210627164911907.png)

## 网络失败

![image-20210627161039660](笔记.assets/image-20210627161039660.png)

# 数据成功获取时的优化

不希望每次成功都res.data.data 地获取, 所以在拦截器先解构

![image-20210627165903210](笔记.assets/image-20210627165903210.png)

![image-20210627165919085](笔记.assets/image-20210627165919085.png)

# 完善登录交互逻辑

![image-20210627174025616](笔记.assets/image-20210627174025616.png)

# 导航守卫拦截 token 

## 逻辑一

![image-20210627180116964](笔记.assets/image-20210627180116964.png)

## 逻辑二

![image-20210627181209055](笔记.assets/image-20210627181209055.png)

## 校验数组中是否存在某个元素

除了 indexOf 还可以用 includes

![image-20210627181736679](笔记.assets/image-20210627181736679.png)

# git 地址提交列表
https://www.kdocs.cn/l/srwkx20bsaD6?f=111
[文档] 63期人资系统 git 地址列表.xlsx